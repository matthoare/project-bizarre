beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.fixture('projectOutline').as('outlines')
  cy.restoreLocalStorage();
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to create a project outline', () => {
  //Login into staff section
  cy.login()
});

And('I click on create an project outline', () => {
  //Cannot see other staff outlines and click create an example
  cy.create('Project Outlines', 'ProjectOutline', 'projectoutlines')
});
//Varibles needed for next sets of tests
const ProjectTitle = 'Project Outline 1'
const ProjectRoom = 'Room 1'
const ProjectDegree = 'Degree 1'
const ProjectCategory = 'Category 1'
const ProjectOutline = 'Outline 1'
const ProjectLink = 'https://link.springer.com/content/pdf/10.1023/A:1013184611077.pdf'

When('I fill out the form to create a project outline', () => {
  //Checking whether on the project outline create page
  cy.createForm('Project Outlines', 'projectoutlines')
  //For the required fields
  cy.required('projectoutlines')
  //Filling out the create form
  cy.get('[data-cy=title]').type(ProjectTitle)
  cy.get('[data-cy=room]').type(ProjectRoom)
  cy.get('[data-cy=degree]').type(ProjectDegree)
  cy.get('[data-cy=outline]').type(ProjectOutline)
  cy.get('[data-cy=link]').type(ProjectLink)
  //Upload a PDF
  const pdf = 'example.pdf';
  cy.get('[data-cy="pdf-input"]').attachFile(pdf);
});

And('I click Submit button to create project outline', () => {
  //Send the form to the server stubbing the API required
  cy.route({
      method: 'POST',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  cy.get('[data-cy=submit]').click() //Submit Form
});

Then('Check the new project outline has been created and can be seen on the dashboard', () => {
  //Get the article from fixtures and from the db to display
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    }).as('outliness')
  //Check have been redirected to the correct page
  cy.url().should('be.equal', 'http://localhost:3000/staff/projectoutlines')
  cy.title().should('be.equal', 'Project Outlines')
  cy.contains('h1', 'Project Outlines')
  // cy.message('Project Outline', 'Created')
  //Check the outline created is being displayed on the page
  cy.get('[data-cy=ProjectOutlinesTable] > tbody > tr:nth-child(1) > th:nth-child(1)').contains('Title')
  cy.get('[data-cy=ProjectOutlinesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > a > p').contains(ProjectTitle)
  cy.get('[data-cy=ProjectOutlinesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > p:nth-child(2)').contains('Created At:')
  cy.get('[data-cy=ProjectOutlinesTable] > tbody > tr:nth-child(1) > th:nth-child(2)').contains('Author')
  cy.get('[data-cy=ProjectOutlinesTable] > tbody > tr:nth-child(2) > td:nth-child(2)').contains('Admin')
  cy.logout()
})
