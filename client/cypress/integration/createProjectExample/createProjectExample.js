beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.fixture('projectExample').as('examples')
  cy.restoreLocalStorage();
})
afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to create project example', () => {
  //Login into staff section
  cy.login()
});

And('I click on create an project example', () => {
  //Cannot see other staff examples and click create an example
  cy.create('Project Examples', 'ProjectExample', 'projectexamples')
});
//Varibles needed for next sets of tests
const ProjectTitle = 'Project Example 1'
const ProjectOutline = 'Outline 1'
const ProjectSkills = 'Skills 1'
const ProjectCategory = 'Categories 1'
const ProjectContent = 'Content 1'
const ProjectImgAlt = 'Image Alt'
const ProjectVideo = 'https://www.youtube.com/embed/sBP9H43gEwg'

When('I fill out the form to create a project example', () => {
  //Checking whether on project example create page
  cy.createForm('Project Examples', 'projectexamples')
  //For the required fields
  cy.required('projectexamples')
  //Filling out the create form
  cy.get('[data-cy=title]').type(ProjectTitle)
  cy.get('[data-cy=outline]').type(ProjectOutline)
  cy.get('[data-cy=category]').type(ProjectCategory)
  cy.get('[data-cy=skills]').type(ProjectSkills)
  cy.get('[data-cy=content]').type(ProjectContent)
  //Upload an Image
  const image = 'example.jpg';
  cy.get('[data-cy="image-input"]').attachFile(image);
  cy.get('[data-cy=alt]').type(ProjectImgAlt)
  cy.get('[data-cy=video]').type(ProjectVideo)
});

And('I click Submit button to create a project example', () => {
  //Send the form to the server stubbing the API required
  cy.route({
      method: 'POST',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    })
  cy.get('[data-cy=submit]').click() //Submit Form
});

Then('Check the new project example has been created and can be seen on the dashboard', () => {
  //Get the article from fixtures and from the db to display
  cy.route({
      method: 'GET',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    }).as('exampless')
  //Check have been redirected to the correct page
  cy.url().should('be.equal', 'http://localhost:3000/staff/projectexamples')
  cy.title().should('be.equal', 'Project Examples')
  cy.contains('h1', 'Project Examples')
  // cy.message('Project Example', 'Created')
  //Check the example created is being displayed on the page
  cy.get('[data-cy=ProjectExamplesTable] > tbody > tr:nth-child(1) > th:nth-child(1)').contains('Title')
  cy.get('[data-cy=ProjectExamplesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > a > p').contains(ProjectTitle)
  cy.get('[data-cy=ProjectExamplesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > p:nth-child(2)').contains('Created At:')
  cy.get('[data-cy=ProjectExamplesTable] > tbody > tr:nth-child(1) > th:nth-child(2)').contains('Author')
  cy.get('[data-cy=ProjectExamplesTable] > tbody > tr:nth-child(2) > td:nth-child(2)').contains('Admin')
  cy.logout()
})
