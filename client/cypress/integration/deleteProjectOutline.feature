Feature: Delete A Project Outline
  As a staff
  I want to delete a project outline

    Scenario: To delete an Project Outline
      Given I am on the staff dashboard to delete a project outline
      When I delete an project outline
      Then Project outline can no longer be seen on the dashboard
