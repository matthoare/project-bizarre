Given('I am on the staff login', () => {
  //Click onto staff login
  cy.visit('/articles')
  cy.get('#mobileNav > li:nth-child(4) > a').click()
  //Check on the correct page
  cy.url().should('be.equal', 'http://localhost:3000/staff/login')
  cy.title().should('be.equal', 'Staff Login')
  //Staff element should show the user that their on that page
  cy.contains('a', 'Staff').should('have.class', 'nuxt-link-active')
  cy.contains('h1', 'Login')
});

And('I click on sign up', () => {
  //Click the sign up button
  cy.get('[data-cy=signUp]').click()
})

const UserName = 'Admin'
const UserEmail = 'admin@example.co.uk'
const UserPassword = 'password'

When('I fill out the form', () => {
  cy.url().should('be.equal', 'http://localhost:3000/staff/register')
  cy.title().should('be.equal', 'Register')
  cy.contains('Register')
  //Call the required command to show that the required fields
  cy.required('users')
  //Fill out the form to creare an account
  cy.contains('label', 'Name')
  cy.contains('label', 'Email')
  cy.contains('label', 'Password')
  cy.get('[data-cy=name]').type(UserName)
  cy.get('[data-cy=email]').type(UserEmail)
  cy.get('[data-cy=password]').type(UserPassword)
});

And('I click on sign up button', () => {
  //Submit the form
  cy.get('[data-cy=submit]').click()
})

Then('I am on the staff dashboard', () => {
  //Redirected to a new page which is the staff dashboard
  cy.url().should('be.equal', 'http://localhost:3000/staff/dashboard')
  cy.title().should('be.equal', 'Dashboard')
  cy.contains('h1', 'Dashboard')
  cy.contains('Logout').and('be.visible')
  cy.contains('Staff').should('not.visible')
  //Logout of the staff section
  cy.logout()
});
