Feature: Login
  As a staff
  I want to login

    Scenario: To update an project example
      Given I am on the home page to login
      And I click on staff link
      And I fill out of the wrong username and password
      When I fill out of the correct username and password
      And I click on login button
      Then I am on the staff dashboard home page
