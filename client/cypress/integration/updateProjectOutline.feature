Feature: Update A Project Outline
  As a staff
  I want to update a project outline

    Scenario: To update an Article
      Given I am on the staff dashboard to update a project outline
      And I click on the update project outline button
      When I update the title in the project ouline form
      And I click on the update project outline submit button
      Then Check the project outline has been updated and can be seen on the dashboard
