import axios from 'axios'

const article_url = 'http://localhost:3001/articles'

export const state = () => ({
  //Array of all the articles
  articles: [],
  //Object of just one array
  article: {},
})

export const actions = {
  async viewArticles({commit}, id) {
    try {
      await axios.get('/api/articles')
        .then(response => {
         const articles = response.data
         commit('SET_ARTICLES', articles)
       })
   } catch(error) {
     console.error(error)
     }
   },
   async createArticle({commit}, payload) {
     try {
       await axios.post('/api/articles', payload)
       //Display message that the article title has been created
       this.$toast.success(payload.title + ' Successfully Created')
     } catch(error) {
       console.error(error)
     }
   },
  async showArticle({commit}, id) {
    try {
      await axios.get('/api/articles/' + id)
       .then(response => {
         const article = response.data;
         commit('SET_ARTICLE', article)
       })
     } catch(error) {
       console.log(error)
     }
   },
   async updateArticle({commit}, payload) {
     try {
       console.log('Hello')
       await axios.put('/api/articles/' + payload.id, payload)
       .then(response => {
         const article = response.data
         this.$toast.success(payload.title + ' Successfully Updated')
         commit('UPDATE_ARTICLE', article)
       })
     } catch(error) {
        console.log(error)
     }
   },
   async deleteArticle({commit}, article) {
     try {
       await axios.delete('/api/articles/' + article.id)
       this.$toast.success(article.title + ' Successfully Deleted')
       commit('DELETE_ARTICLE', article.id)
     } catch(error) {
       console.log(error)
     }
   }
}

export const mutations = {
  SET_ARTICLES(state, articles) {
    state.articles = articles
  },
  SET_ARTICLE(state, article) {
    state.article = article
  },
  UPDATE_ARTICLE(state, article) {
    //Find the article id that has been passed in the state
    const articles = state.articles.find(articles => articles.id == article.id);
    //Update the article by merging the new article into the state
    Object.assign(articles, article)
  },
  DELETE_ARTICLE(state, id) {
  /*
  Get the index of the articles in the state if the article id of that index matches
  the id passed then remove it from the state
  */
  var index = state.articles.findIndex(article => article.id == id)
  state.articles.splice(index, 1)
  }
}
