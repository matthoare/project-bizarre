Feature: Create A Project Outline
  As a staff
  I want to create a project outline

    Scenario: To create an Project Outline
      Given I am on the staff dashboard to create a project outline
      And I click on create an project outline
      When I fill out the form to create a project outline
      And I click Submit button to create project outline
      Then Check the new project outline has been created and can be seen on the dashboard
