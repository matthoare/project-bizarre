> Website about final year projects for students

## Build Setup

```bash

Go into the client directory within this folder run the following two commands
which will install the dependencies and serve the site.

# install dependencies
$ npm install
# serve with hot reload at localhost:3000
$ npm run dev

To start the backend server open up a new terminal and go into the
server directory within this folder and run the following command.

# start the backend server
node index.js

To login as a Staff member to view the content created the credentials are:
#Login Details
email: admin@example.co.uk
password: password
