beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
  cy.fixture('allProjectExample').as('examples')
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard tp delete a project example', () => {
  cy.route({
      method: 'GET',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    })
  //Login into staff section
  cy.login()
});

When('I delete an project example', () => {
  //Get the articles from fixture and stub the request
  cy.route({
      method: 'GET',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    })
  cy.updateDelete('Project Examples', 'ProjectExamples', 'projectexamples', '[data-cy=ProjectExamplesTable]')
  cy.route({
      method: 'DELETE',
      url: '/api/projectexamples/1',
      response: '@examples',
      status: 200
    })
  //Click on see all examples and click on delete on the first one
  cy.clickDelete('projectexamples', 'Project Example 1')
});

Then('project example can no longer be seen on the dashboard', () => {
  //The example is deleted and cannot be seen
  cy.confirmDelete('Project Examples', 'projectexamples', 'Project Example 1')
  cy.logout()
})
