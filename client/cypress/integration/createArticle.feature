Feature: Create An Article
  As a staff
  I want to create an Article

    Scenario: To create an Article
      Given I am on staff dashboard to create article
      And I click on create an article
      When I fill out the create article form
      And I click Submit button to create article
      Then Check the new article has been created and can be seen on the dashboard
