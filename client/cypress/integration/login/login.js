beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.fixture('user').as('users')
})

Given('I am on the home page to login', () => {
  //On the home page
  cy.visit('/articles')
  cy.contains('h1', 'Articles')
})

And('I click on staff link', () => {
  cy.get('[data-cy=staff]').contains('Staff').click()
})

And('I fill out of the wrong username and password', () => {
  cy.url().should('eq', 'http://localhost:3000/staff/login')
  cy.title().should('be.equal', 'Staff Login')
  cy.contains('a', 'Staff').should('have.class', 'nuxt-link-active')
  cy.contains('h1', 'Staff Login')

  //Check the required fields
  cy.required('users')

  //Invalid Users Details
  const InvalidUserEmail = 'admin@example.com'
  const InvalidUserPassword = 'password123'

  cy.get('[data-cy=email]').type(InvalidUserEmail)
  cy.get('[data-cy=password]').type(InvalidUserPassword)
  cy.get('[data-cy=submit]').click()
  cy.url().should('be.equal', 'http://localhost:3000/staff/login')
})

When('I fill out of the correct username and password', () => {
  //Valid Users Details
  const UserName = 'Admin'
  const UserEmail = 'admin@example.co.uk'
  const UserPassword = 'password'

  //Valid
  cy.get('[data-cy=email]').should('be.empty')
  cy.get('[data-cy=password]').should('be.empty')
  cy.get('[data-cy=email]').type(UserEmail)
  cy.get('[data-cy=password]').type(UserPassword)
})

And('I click on login button', () => {
  //Click Signin
  cy.get('[data-cy=submit]').click()
})

Then('I am on the staff dashboard home page', () => {
  cy.url().should('be.equal', 'http://localhost:3000/staff/dashboard')
  cy.contains('h1', 'Dashboard')
  cy.contains('Staff').should('not.visible')
  cy.contains('Logout').should('be.visible')
  cy.logout()
})
