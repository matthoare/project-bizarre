beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
  cy.fixture('allArticles').as('articles')
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to delete an article', () => {
  cy.route({
      method: 'GET',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  //Login into staff section
  cy.login()
});

When('I delete an article', () => {
  //Get the articles from fixture and stub the request
  cy.route({
      method: 'GET',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  cy.updateDelete('Articles', 'Articles', 'articles', '[data-cy=ArticlesTable]')
  cy.route({
      method: 'DELETE',
      url: '/api/articles/1',
      response: '@articles',
      status: 200
    })
  //Click on see all articles and click on delete on the first one
  cy.clickDelete('articles', 'Article 1')
});

Then('Article can no longer be seen on the dashboard', () => {
  //The article is deleted and cannot be seen
  cy.confirmDelete('Articles', 'articles', 'Article')
  cy.logout()
})
