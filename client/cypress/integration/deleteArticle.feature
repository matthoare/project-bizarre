Feature: Delete An Article
  As a staff
  I want to delete an article

    Scenario: To delete an Article
      Given I am on the staff dashboard to delete an article
      When I delete an article
      Then Article can no longer be seen on the dashboard
