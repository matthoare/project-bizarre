import axios from 'axios'

export const state = () => ({
  outlines: [],
  outline: {}
})

export const actions = {
  async viewOutlines({commit}) {
    try {
      axios.get('/api/projectoutlines')
      .then(response => {
       const outlines = response.data
       commit('SET_OUTLINES', outlines)
     })
   } catch(error) {
       console.log(error)
     }
   },
   async createOutline(context, payload) {
     try {
       await axios.post('/api/projectoutlines', payload)
       this.$toast.success(payload.title + ' Successfully Created')
     } catch(error) {
       console.error(error)
     }
   },
   async showOutline({commit}, id) {
     try {
       await axios.get('/api/projectoutlines/' + id)
       .then(response => {
         const outline = response.data
         commit('SET_OUTLINE', outline)
       })
     } catch(error) {
       console.log(error)
     }
   },
   async updateOutline({commit}, payload) {
     try {
       await axios.put('/api/projectoutlines/' + payload.id, payload)
       .then(response => {
         const outline = response.data
         this.$toast.success(payload.title + ' Successfully Updated')
         commit('UPDATE_OUTLINE', outline)
       })
     } catch(error) {
       console.log(error)
     }
   },
   async deleteOutline({commit}, outline) {
     try {
       await axios.delete('/api/projectoutlines/' + outline.id)
       this.$toast.success(outline.title + ' Successfully Deleted')
       commit('DELETE_OUTLINE', outline.id)
     } catch(error) {
       console.log(error)
     }
   }
 }

 export const mutations = {
   SET_OUTLINES(state, outlines) {
     state.outlines = outlines
   },
   SET_OUTLINE(state, outline) {
     state.outline = outline
   },
   UPDATE_OUTLINE(state, outline) {
     const outlines = state.outlines.find(outlines => outlines.id == outline.id);
     Object.assign(outlines, outline)
   },
   DELETE_OUTLINE(state, id) {
     var index = state.outlines.findIndex(outline => outline.id == id)
     state.outlines.splice(index, 1)
   }
 }
