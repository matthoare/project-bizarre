beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to update a project example', () => {
  cy.fixture('allProjectExample').as('examples')
  cy.route({
      method: 'GET',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    })
  //Login into staff section
  cy.login()
});

And('I click on edit project example', () => {
  //Click on see all examples and click on edit button to edit the first one
  cy.updateDelete('Project Examples', 'ProjectExamples', 'projectexamples', '[data-cy=ProjectExamplesTable]')
  cy.fixture('projectExample').as('example')
  cy.route({
      method: 'GET',
      url: '/api/projectexamples/1',
      response: '@example',
      status: 200
    })
  cy.get('[data-cy=editProjectExample]').click()
});

const ProjectTitle = 'Updated Project Example 1'
When('I update the title in the project example form', () => {
  //Check on correct edit page and update the title
  cy.url().should('be.equal', 'http://localhost:3000/staff/projectexamples/edit/1')
  cy.title().should('be.equal', 'Edit Project Example - Project Example 1')
  cy.contains('h1', 'Edit Project Example - Project Example 1')
  //Clear the title field and submit form
  cy.get('[data-cy=title]').clear()
  // cy.required('projectexamples')
  cy.get('[data-cy=title]').type(ProjectTitle)
});

And('I click on the update project example button', () => {
    cy.get('[data-cy=submit]').click()//Update the outline
});

Then('Check the project example has been updated and can be seen on the dashboard', () => {
  //Check been redirected and the updated example title can be seen
  cy.confirmUpdate('projectexamples', '@examples', ProjectTitle)
  cy.logout()
})
