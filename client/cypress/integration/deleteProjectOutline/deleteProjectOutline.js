beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
  cy.fixture('singleProjectOutline').as('outlines')
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to delete a project outline', () => {
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  //Login into staff section
  cy.login()
});

When('I delete an project outline', () => {
  //Get the articles from fixture and stub the request
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  cy.updateDelete('Project Outlines', 'ProjectOutlines', 'projectoutlines', '[data-cy=ProjectOutlinesTable]')
  cy.route({
      method: 'DELETE',
      url: '/api/projectoutlines/1',
      response: '@outlines',
      status: 200
    })
  //Click on see all outlines and click on delete on the first one
  cy.clickDelete('projectoutlines', 'Project Outline 1')
});

Then('Project outline can no longer be seen on the dashboard', () => {
  //The outline is deleted and cannot be seen
  cy.confirmDelete('Project Outlines', 'projectoutlines', 'Project Outline 1')
  cy.logout()
})
