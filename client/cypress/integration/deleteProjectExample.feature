Feature: Delete A Project Example
  As a staff
  I want to delete a project example

    Scenario: To delete an project example
      Given I am on the staff dashboard tp delete a project example
      When I delete an project example
      Then project example can no longer be seen on the dashboard
