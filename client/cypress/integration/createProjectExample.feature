Feature: Create A Project Example
  As a staff
  I want to create a project example

    Scenario: To create an project example
      Given I am on the staff dashboard to create project example 
      And I click on create an project example
      When I fill out the form to create a project example
      And I click Submit button to create a project example
      Then Check the new project example has been created and can be seen on the dashboard
