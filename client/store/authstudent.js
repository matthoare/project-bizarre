export const state = () => ({
  authenticated: false,
  path: ''
})

export const actions = {
  checkEmail({commit}, email) {
    var number = email.split('@');
    var ehuemail = "edgehill.ac.uk"
    //If the email matches edge hill then allow access
    if(number[1] == ehuemail) {
      //Direct to the request path from the navigation
      this.$router.push({path: state.path})
      commit('SET_CORRECT_EMAIL')
    } else {
      this.$toast.error('Invalid Email')
    }
  },
  //Set the state of the path to the link passed through
  redirectPath({commit}, path) {
    state.path = path
  }
}

export const mutations = {
  SET_CORRECT_EMAIL(state) {
    state.authenticated = true
  }
}
