beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to update a project outline', () => {
  cy.fixture('singleProjectOutline').as('outlines')
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  //Login into staff section
  cy.login()
});

And('I click on the update project outline button', () => {
  cy.fixture('singleProjectOutline').as('outlines')
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  //Click on see all examples and click on edit button to edit the first one
  cy.updateDelete('Project Outlines', 'ProjectOutlines', 'projectoutlines', '[data-cy=ProjectOutlinesTable]')
  cy.fixture('projectOutline').as('outline')
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines/1',
      response: '@outline',
      status: 200
    })
  cy.get('[data-cy=editProjectOutline]').click()
});

const ProjectTitle = 'Updated Project Outline 1'
When('I update the title in the project ouline form', () => {
  //Check on correct edit page and update the title
  cy.url().should('be.equal', 'http://localhost:3000/staff/projectoutlines/edit/1')
  cy.title().should('be.equal', 'Edit Project Outline - Project Outline 1')
  cy.contains('h1', 'Edit Project Outline - Project Outline 1')
  //Clear the title field and submit form
  cy.get('[data-cy=title]').clear()
  // cy.required('projectoutlines')
  cy.get('[data-cy=title]').type(ProjectTitle)
});

And('I click on the update project outline submit button', () => {
  cy.route({
      method: 'PUT',
      url: '/api/projectoutlines/1',
      response: {title: ProjectTitle},
      status: 200
    }).as('outliness')
  cy.get('[data-cy=submit]').click()//Update the outline
});

Then('Check the project outline has been updated and can be seen on the dashboard', () => {
  //Check been redirected and the updated outline title can be seen
  cy.confirmUpdate('projectoutlines', '@outlines', ProjectTitle)
  cy.logout()
})
