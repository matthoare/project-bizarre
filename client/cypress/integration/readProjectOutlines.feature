Feature: Read Project Outlines
  As a student
  I want read to project outlines

  Scenario: To read project outlines
    Given I am on the home page to read a project outline
    And I click on Project outlines link
    When I login to view on a project outline
    And I can read the project outlines details
    Then I select a degree and catgeory
