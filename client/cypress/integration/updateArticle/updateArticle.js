beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.restoreLocalStorage();
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on the staff dashboard to update an article', () => {
  cy.fixture('allArticles').as('articles')
  cy.route({
      method: 'GET',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  //Login into staff section
  cy.login()
});

And('I click on edit article', () => {
  cy.route({
      method: 'GET',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  //Click on see all article and click on edit button to edit the first one
  cy.updateDelete('Articles', 'Articles', 'articles', '[data-cy=ArticlesTable]')
  cy.fixture('article').as('articles')
  cy.route({
      method: 'GET',
      url: '/api/articles/1',
      response: '@articles',
      status: 200
    })
  cy.get('[data-cy=editArticle]').click()
});

const ArticleTitle = 'Updated Article 1'
When('I update the title in the article form', () => {
  //Check on correct edit page and update the title
  cy.url().should('be.equal', 'http://localhost:3000/staff/articles/edit/1')
  cy.title().should('be.equal', 'Edit Article - Article 1')
  cy.contains('h1', 'Edit Article - Article 1')
  // cy.required('articles')
  cy.get('#title').type(ArticleTitle)
});

And('I click on the update article button', () => {
  cy.route({
      method: 'PUT',
      url: '/api/articles/1',
      response: {title: ArticleTitle},
      status: 200
    }).as('articless')
  cy.get('[data-cy=submit]').click()//Update the article
});

Then('Check the article has been updated and can be seen on the dashboard', () => {
  //Check been redirected and the updated article title can be seen
  cy.confirmUpdate('articles', '@articles', ArticleTitle)
  cy.logout()
})
