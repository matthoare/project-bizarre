beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.fixture('allArticles').as('articles')
  cy.restoreLocalStorage();
})

afterEach(() => {
  cy.saveLocalStorage();
});

Given('I am on staff dashboard to create article', () => {
  //Login into staff section
  cy.login()
});

And('I click on create an article', () => {
  //Cannot see other staff articles and click on create an article and the url matches
  cy.create('Articles', 'Article', 'articles')
});
//Varibles needed for next sets of tests
const ArticleTitle = 'Article 1'
const ArticleContent = 'Aritcle Content'
const ArticleImgAlt = 'Image Alt'

When('I fill out the create article form', () => {
  //Checking whether on article create page
  cy.createForm('Article', 'articles')
  //For the required fields
  cy.required('articles')
  //Filling out the create form
  cy.get('[data-cy=title]').type(ArticleTitle)
  cy.get('[data-cy=content]').type(ArticleContent)
  //Upload an Image
  const image = 'example.jpg';
  cy.get('[data-cy="image-input"]').attachFile(image);
  cy.get('[data-cy=alt]').type(ArticleImgAlt)
  //Upload a PDF
  const pdf = 'example.pdf';
  cy.get('[data-cy="pdf-input"]').attachFile(pdf);
});

And('I click Submit button to create article', () => {
  //Send the form to the server stubbing the API required
  cy.route({
      method: 'POST',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  cy.get('[data-cy=submit]').click() //Submit Form
});

Then('Check the new article has been created and can be seen on the dashboard', () => {
  //Send the form to the server stubbing the API required
  cy.route({
      method: 'GET',
      url: '/api/articles',
      response: '@articles',
      status: 200
    })
  //Check have been redirected to the correct page
  cy.url().should('be.equal', 'http://localhost:3000/staff/articles')
  cy.title().should('be.equal', 'Articles')
  cy.contains('h1', 'Articles')
  // cy.message('Article', 'Created')
  //Check the article created is being displayed on the page
  cy.get('[data-cy=ArticlesTable] > tbody > tr:nth-child(1) > th:nth-child(1)').contains('Title')
  cy.get('[data-cy=ArticlesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > a > p').contains(ArticleTitle)
  cy.get('[data-cy=ArticlesTable] > tbody > tr:nth-child(2) > td:nth-child(1) > p:nth-child(2)').contains('Created At:')
  cy.get('[data-cy=ArticlesTable] > tbody > tr:nth-child(1) > th:nth-child(2)').contains('Author')
  cy.get('[data-cy=ArticlesTable] > tbody > tr:nth-child(2) > td:nth-child(2)').contains('Admin')
  cy.logout()
})
