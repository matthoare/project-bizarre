Given('I am on the home page to read a project outline', () => {
  cy.visit('/articles')
  //Check the home page contains these elements
  cy.title().should('be.equal', 'Articles')
  cy.contains('h1', 'Articles')
});

And('I click on Project outlines link', () => {
  //Click on the project outlines link
  cy.get('#mobileNav > li:nth-child(2) > a').click()
});

When('I login to view on a project outline', () => {
  //Redirected to the student login
  cy.studentLogin()
  cy.server()
  cy.fixture('allProjectOutlines').as('outlines')
  cy.route({
      method: 'GET',
      url: '/api/projectoutlines',
      response: '@outlines',
      status: 200
    })
  cy.get('[data-cy=submit]').contains('Login').click()
});

And('I can read the project outlines details', () => {
  //Been redirected to the correct section
  cy.url().should('be.equal', 'http://localhost:3000/student/projectoutlines')
  cy.title().should('be.equal', 'Project Outlines')
  cy.contains('a', 'Project Outlines').should('have.class', 'nuxt-link-active')
  cy.contains('h1', 'Project Outlines')
  //Checking the contents of the card
  cy.contains('h2', 'Project Outline 1')
  cy.contains('p', 'Admin')
  cy.contains('p', 'Room 1')
  cy.contains('p', 'Outline 1')
  cy.contains('a', 'https://link.springer.com/content/pdf/10.1023/A:1013184611077.pdf')
  cy.contains('a').should('have.attr', 'href', )
  //Check email has link to click on
  cy.get('#__layout > div > main > section > article:nth-child(2) > section > a').should('have.attr', 'href')
});

Then('I select a degree and catgeory', () => {
  cy.contains('Project Outline 1')
  //Select a degree from the drop down
  cy.contains('label', 'Degree')
  cy.contains('select', 'Web Design And Development').select('Web Design And Development')
  cy.contains('Web Design And Development 1').should('be.visible')
  cy.contains('Project Outline 1').should('not.visible')

});
