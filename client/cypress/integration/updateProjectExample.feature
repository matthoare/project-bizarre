Feature: Update A Project Example
  As a staff
  I want to update an project example

    Scenario: To update an project example
      Given I am on the staff dashboard to update a project example
      And I click on edit project example
      When I update the title in the project example form
      And I click on the update project example button
      Then Check the project example has been updated and can be seen on the dashboard
