import axios from 'axios'

export const actions = {
  async saveUser(context, payload) {
    try {
      await this.$axios.post('/api/register', payload)
    } catch (error) {
      console.error(error)
    }
  }
}
