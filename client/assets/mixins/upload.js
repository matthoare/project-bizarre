export default {
  methods : {
    //Turns the file into a base64 to store in json
    onFileSelected(e) {
      var file = e.target.files[0];//Get file from input
      var reader = new FileReader();//Turn file into base 64
      //If file type is jpeg or png then assign this to selected file
      if(file['type'] === 'image/jpeg' || file['type'] === 'image/png') {
        reader.onloadend = () => {
          this.image = URL.createObjectURL(file)
          this.image = reader.result;
        }
      }
      //If its not a picture then it is a pdf
      else {
          reader.onloadend = () => {
          this.pdf = reader.result;
        }
      }
      reader.readAsDataURL(file);
    }
  }
}
