const title = name => ({
  head() {
    return {
      title: name
    }
  }
})

export default title
