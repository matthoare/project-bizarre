import axios from 'axios'


export const state = () => ({
  examples: [],
  example: {}
})

export const actions = {
  async viewExamples({commit}) {
    try {
      await axios.get('/api/projectexamples')
        .then(response => {
         const examples = response.data
         commit('SET_EXAMPLES', examples)
       })
     } catch(error) {
       console.log(error)
       }
   },
   async createExample({commit}, payload) {
     try {
       await axios.post('/api/projectexamples', payload)
       this.$toast.success(payload.title + ' Successfully Created')
       commit('ADD_EXAMPLE', payload)
     } catch(error) {
       console.log(error)
     }
   },
   async showExample({commit}, id) {
     try {
       await axios.get('/api/projectexamples/' + id)
        .then(response => {
          const example = response.data;
          commit('SET_EXAMPLE', example)
        })
      } catch(error) {
        console.log(error)
    }
  },
  async updateExample({commit}, payload) {
    try {
      await axios.put('/api/projectexamples/' + payload.id, payload)
      .then(response => {
        const example = responsive.data
        this.$toast.success(payload.title + ' Successfully Updated')
        commit('UPDATE_EXAMPLE', example)
      })
    } catch(error) {
      console.log(error)
    }
  },
  async deleteExample({commit}, example) {
    try {
      await axios.delete('/api/projectexamples/' + example.id)
      this.$toast.success(example.title + ' Successfully Deleted')
      commit('DELETE_EXAMPLE', example.id)
    } catch(error) {
      console.log(error)
    }
  }
}

export const mutations = {
  SET_EXAMPLES(state, examples) {
    state.examples = examples
  },
  SET_EXAMPLE(state, example) {
    state.example = example
  },
  ADD_EXAMPLE(state, payload)  {
    const examples = state.examples.concat(payload);
    state.examples = examples
  },
  UPDATE_EXAMPLE(state, example) {
    const examples = state.examples.find(examples => examples.id == example.id);
    Object.assign(examples, example)
  },
  DELETE_EXAMPLE(state, id) {
    var index = state.examples.findIndex(example => example.id == id)
    state.examples.splice(index, 1)
  }
}
