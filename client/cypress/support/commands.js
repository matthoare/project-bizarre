import 'cypress-file-upload'

//Get Requests
Cypress.Commands.add("getRequest", (section, json) => {
  cy.server()
  cy.route({
      method: 'GET',
      url: '/api/' + section,
      response: json,
      status: 200
    })
})

//Login
Cypress.Commands.add("login", () => {
  cy.visit('/staff/login')
  cy.get('[data-cy=email]').type('admin@example.co.uk')
  cy.get('[data-cy=password]').type('password')
  cy.get('[data-cy=submit]').click()
  })

/*
Due to cypress clearing the local storage after each it test this meant the user is logged
out after it test. The following code below has been taken from https://github.com/cypress-io/cypress/issues/461#issuecomment-392070888
to solve this issue.
*/
let LOCAL_STORAGE_MEMORY = {};

Cypress.Commands.add("saveLocalStorage", () => {
  Object.keys(localStorage).forEach(key => {
    LOCAL_STORAGE_MEMORY[key] = localStorage[key];
  });
});

Cypress.Commands.add("restoreLocalStorage", () => {
  Object.keys(LOCAL_STORAGE_MEMORY).forEach(key => {
    localStorage.setItem(key, LOCAL_STORAGE_MEMORY[key]);
  });
});


//Student Login
Cypress.Commands.add("studentLogin", (link) => {
  //Click Project Example or List
  cy.url().should('be.equal', 'http://localhost:3000/student/login')
  cy.title().should('be.equal', 'Student Login')
  cy.contains('h1', 'Student Login')
  cy.contains('label', 'Please enter your Edge Hill email')

  //Required fields
  cy.get('[data-cy=submit]').click()
  cy.contains('p', 'Email is required').and('be.visible')

  //Invalid
  cy.get('[data-cy=email]').type('student@hotmail.co.uk')
  cy.get('[data-cy=submit]').click()
  cy.url().should('be.equal', 'http://localhost:3000/student/login')

  //Valid
  cy.get('[data-cy=email').clear()
  cy.get('[data-cy=email').type('student@edgehill.ac.uk')
})

/*
Key:
heading = Articles, Project Examples & Project Lists
section = articles, projectexamples, projectlist
table = CRUD Table
title = Title of document e.g. Article 1, Project Example 1, Project Outline 1
*/

//Clicking Different Links
Cypress.Commands.add("links", (section, json, title, data) => {
  cy.getRequest(section, json);
  //Go back to previous page
  cy.go(-1)
  //Check it has navigated back to the correct page
  cy.title().should('be.equal', title)
  //Method 3 of clicking on article
  cy.get(data).click()
})

//Click Create
Cypress.Commands.add("create", (heading, button, href) => {
  cy.contains('h1', 'Latest ' + heading)
  //Clicking Create Button
  cy.get('[data-cy=create' + button + ']').should('have.class', 'create').contains('Create ' + heading).click()
})

//Click Update / Delete
Cypress.Commands.add("updateDelete", (heading, button, section, table) => {
  cy.get('[data-cy=seeAll' + button + ']').should('have.class', 'seeAll').contains('See All ' + heading).click()
  //Check Titles
  cy.url().should('be.equal', 'http://localhost:3000/staff/' + section)
  cy.title().should('be.equal', heading)
  cy.contains('h1', 'All ' + heading)
  //Check Table
  cy.get(table + ' > tbody > tr:nth-child(1) > th:nth-child(3)').contains('Edit')
  cy.get(table + ' > tbody > tr:nth-child(1) > th:nth-child(4)').contains('Delete')
})

//Required Fields
Cypress.Commands.add("required", (section) => {
  cy.get("[data-cy=submit]").click()
  cy.contains('p', 'required').should('be.visible')
})

//Create Form
Cypress.Commands.add("createForm", (heading, section) => {
  cy.url().should('be.equal', 'http://localhost:3000/staff/' + section + '/create')
  cy.title().should('be.equal', 'Create ' + heading)
  cy.contains('h1', 'Add ' + heading)
})

//Update Form
Cypress.Commands.add("updateForm", (section, heading, title) => {
  cy.url().should('be.equal', '/staff/' + section + '/edit/1')
  cy.title().should('be.equal', 'Edit ' + heading + ' - ' + title)
  cy.get('[data-cy=title]').type('Updated ' + title)
})

//Message
Cypress.Commands.add("message", (section, crud) => {
  //Displays message on the screen
  cy.get('.toasted.toasted-primary.success').contains(section + ' Was Successfully ' + crud)
})

//Confirm Update
Cypress.Commands.add("confirmUpdate", (section, json, title) => {
  cy.server()
  cy.route({
      method: 'GET',
      url: '/api/' + section + '/1',
      response: {title: title},
      status: 200
    })
  cy.url().should('be.equal', 'http://localhost:3000/staff/' + section + '/1')
  cy.title().should('be.equal', title)
  cy.contains('h1', title)
})

//Click Delete
Cypress.Commands.add("clickDelete", (section, title) => {
  cy.route({
      method: 'DELETE',
      url: 'http://localhost:3000/db/' + section + '/1',
      response: '',
      status: 200
    })
  cy.get('[data-cy=delete]').click()
  cy.on('window:confirm', (str) => {
  expect(str).to.contains('Are you sure you want to delete ' + title)
})
})

//Confirm Delete
Cypress.Commands.add("confirmDelete", (heading, section, title) => {
  cy.title().should('be.equal', heading)
  cy.url().should('be.equal', 'http://localhost:3000/staff/' + section)
  cy.contains('td', title).should('not.visible')
})

//Logout
Cypress.Commands.add("logout", () => {
  cy.get('[data-cy=logout]').should('be.visible').click()
  cy.url().should('be.equal', 'http://localhost:3000/articles')
  cy.contains('Staff').should('be.visible')
  cy.contains('Logout').should('not.visible')
})
