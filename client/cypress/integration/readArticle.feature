Feature: Read An Article
  As a student
  I want to read an article

  Scenario: To read an articles
    Given I am on the home page of articles
    When I click on an article to read
    Then I can then read the article
