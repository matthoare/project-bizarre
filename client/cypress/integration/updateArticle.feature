Feature: Update An Article
  As a staff
  I want to update an article

    Scenario: To update an Article
      Given I am on the staff dashboard to update an article
      And I click on edit article
      When I update the title in the article form
      And I click on the update article button
      Then Check the article has been updated and can be seen on the dashboard
