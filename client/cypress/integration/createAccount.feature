Feature: Create An Account
  As a staff
  I want to create an account

    Scenario: To create an account
      Given I am on the staff login
      And I click on sign up
      When I fill out the form
      And I click on sign up button
      Then I am on the staff dashboard
