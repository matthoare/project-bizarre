beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
  cy.fixture('allProjectExample').as('examples')
  cy.route({
      method: 'GET',
      url: '/api/projectexamples',
      response: '@examples',
      status: 200
    })
})

Given('I am on the home page to read a project example', () => {
  cy.visit('/articles')
  //Check the home page contains these elements
  cy.title().should('be.equal', 'Articles')
  cy.contains('h1','Articles')
});

And('I click on Project Examples link', () => {
  //Click on the project examples link
  cy.get('#mobileNav > li:nth-child(3) > a').click()
});

And('I login to view on a project example', () => {
  //Redirected to the student login
  cy.studentLogin()
  cy.get('[data-cy=submit]').click()
});

When('I can view all project examples', () => {
  //Been redirected to the correct section
  cy.url().should('be.equal', 'http://localhost:3000/student/projectexamples')
  cy.title().should('be.equal', 'Project Examples')
  cy.contains('a', 'Project Examples').should('have.class', 'nuxt-link-active')
  cy.contains('h1', 'Project Examples')
  //See if image has loaded
  cy.get('main').find('img').should('have.attr', 'alt')
  //Titles of Project Examples
  cy.contains('h2', 'Project Example 1')
  cy.contains('p', 'Category 1')
  cy.contains('p', 'Outline 1')
});

And('Click on a project example', () => {
  cy.fixture('projectExample').as('example')
  cy.route({
      method: 'GET',
      url: '/api/projectexamples/1',
      response: '@example',
      status: 200
    })
  //Method 1 of clicking on article
  cy.get('[data-cy=readMore]').click()
  cy.title().should('be.equal', 'Project Example 1')
  // Check it has navigated back to the correct page
  cy.go(-1)
  cy.title().should('be.equal', 'Project Examples')
  //Method 2 of clicking on project example
  cy.get('[data-cy=exampleTitle]').click()
  cy.title().should('be.equal', 'Project Example 1')
  cy.go(-1)
  cy.title().should('be.equal', 'Project Examples')
  //Method 3 of clicking on project example
  cy.get('[data-cy=exampleImg]').click()
  cy.title().should('be.equal', 'Project Example 1')
})

Then('I can read the project example details', () => {
  //Been redirected to the correct project example page
  cy.url().should('be.equal', 'http://localhost:3000/student/projectexamples/1')
  cy.title().should('be.equal', 'Project Example 1')
  //See if page contains the elements on the page
  cy.contains('h1', 'Project Example 1')
  cy.get('main').find('img').should('have.attr', 'alt')
  cy.contains('p', 'Outline 1')
  cy.contains('p', 'Skills 1')
  cy.contains('p', 'Category 1')
  cy.contains('p', 'Content 1')
  cy.getRequest('projectexamples', '');
  /*
  Go back to previous page and the request above is empty so therefore
  should see message below
  */
  cy.go(-1)
  cy.contains('No Project Examples Created Yet')
});
