beforeEach(function() {
  //Start the cypress server before each function
  cy.server()
})

Given('I am on the home page of articles', () => {
  cy.fixture('allArticles').as('articles')
  //Command which stubs the get request
  cy.getRequest('articles', '@articles');
  cy.visit('/articles')
  cy.title().should('be.equal', 'Articles')
  //Active state on nav element
  cy.contains('a', 'Articles').should('have.class', 'nuxt-link-active')
  //Check the logo is visible
  cy.contains('Project Bizarre')
  //Check Image has loaded and has alt tag
  cy.get('main').find('img').should('have.attr', 'alt')
  //Check the article heading
  cy.contains('h1','Articles')
  //Check what displays if there are no articles in the state
  cy.contains('h2','Article 1')
  //Check the image is loaded
  cy.get('[data-cy=articleContent]').contains('Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam at maximus tortor. Suspendisse et ipsum sem. Cras finibus odio eu magna auctor accumsan a at')
});

When('I click on an article to read', () => {
  cy.fixture('article').as('articles')
  cy.getRequest('articles/1', '@articles');
  //Method 1 of clicking on article
  cy.get('[data-cy=readMore]').click()
  cy.title().should('be.equal', 'Article 1')
  //Method 2 of clicking on article
  cy.fixture('allArticles').as('articles')
  cy.links('articles', '@articles', 'Articles', '[data-cy=articleTitle]')
  cy.title().should('be.equal', 'Article 1')
  //Method 3 of clicking on article
  cy.fixture('allArticles').as('articles')
  cy.links('articles', '@articles', 'Articles', '[data-cy=articleImg]')
  cy.title().should('be.equal', 'Article 1')
});

Then('I can then read the article', () => {
  //Been redircted to the correct article
  cy.url().should('be.equal', 'http://localhost:3000/articles/1')
  cy.get('div').find('img').should('have.attr', 'alt')
  cy.contains('h1', 'Article 1')
  cy.contains('p', 'Content 1')
  cy.getRequest('articles', '');
  /*
  Go back to previous page and the request above is empty so therefore
  should see message below
  */
  cy.go(-1)
  cy.contains('No Articles Created Yet')
});
