Feature: Read Project Example
  As a student
  I want to read project example

  Scenario: To read project example
    Given I am on the home page to read a project example
    And I click on Project Examples link
    And I login to view on a project example
    When I can view all project examples
    And Click on a project example
    Then I can read the project example details
